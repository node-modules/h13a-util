var meow = {version:'1.0.1'};
var parseArgs = require('minimist');

meow.parseUnixCommand = function(incomingData) {

  let commands = [];

  // clean up and split by pipe
  if (typeof incomingData == 'string') {
    incomingData = incomingData.trim();
    commands = incomingData.split(/(\b|\s)\|\s*/g).map(s => s.trim()).filter(s => s.length);
  }
  // clean up, it is already split.
  else if ($.isArray(incomingData)) {
    commands = incomingData
      .map( s => s.trim() ) // make sure it is a clean string
      .filter( s => s.length ); // get rid of empty elements
  } else {
    // it was not a string, it was not an array
  }

  // at this point we have an array of unix command strings,
  // this is a flat array: ["echo a", "echo b", "echo -n -e Bork! Bork!"];
  // parse command paramaeters.

  // double check validity here as the above code can change


    commands = commands.map(str => {

      var SPACE = meow.guid();
      var SPACER = new RegExp(SPACE, "g");

      var protect = str.replace(/"([^"]*)"/g, function (x, y) {
        return y.replace(/ /g, SPACE);
      });
      var astr = protect.split(/ /).map(s => s.replace(SPACER, ' '));
      var options = parseArgs(astr)||{};
      var cmd = options._.shift();
      var settings = options._||[];
      delete options._;

      return {cmd, data:{settings,options}};
    });



  return commands;

};

 // { prefix: element: number: }
meow.setNumericClassValue = function(options) {
  let string = $(options.element).attr('class')||"";
  let pattern = new RegExp(options.prefix + "([0-9]{1,2})", "g");
  let clean = string.replace(pattern, ' ');
  let result = clean + ' ' + options.prefix + options.number;
  let output = result.replace(/ +/g, ' ');
  $(options.element).attr('class', output);
};

// {prefix: element: }
meow.getNumericClassValue = function(options) {
  let string = $(options.element).attr('class')||"";
  let pattern = new RegExp(options.prefix + "([0-9]{1,2})");
  let response = 0;
  let match = string.match(pattern);
  if(match && match[1]){
    response = match[1];
  }
  return parseInt( response );
};




meow.widgetFromNode = function(namespace, node) {
  const FIELDNAME = 'kind';
  let response = null;
  let $node = $(node);

  if($node){

    let componentKind = $node.data(FIELDNAME);
    let key = namespace + '-' + componentKind;
    response = $node.data( key );

  }

  return response;
};






// meow.mixin = function(Parent, ...mixins) {
//   class Mixed extends Parent {}
//   for (let mixin of mixins) {
//     for (let prop in mixin) {
//       Mixed.prototype[prop] = mixin[prop];
//     }
//   }
//   return Mixed;
// };

    meow.smash = function(data, html){
      let $node = $('<div>'+html+'</div>');
      for (var name in data) {
        if (data.hasOwnProperty(name)) {
          var value = data[name]
          $('.'+name+'-position', $node).html(value);
        }
      }
      return $node.html();
    }

    meow.table = function(head, ...body) {
      var table = `
      <table class="table table-sm">
        <thead>
          <tr>
            ${head.map(th=>`<th>${th}</th>`).join('')}
          </tr>
        </thead>
        <tbody>
          ${body.map(tr=>`<tr>${tr.map(td=>`<td>${td}</td>`).join('')}</tr>`).join('')}
        </tbody>
      </table>`.trim();
      return table;
    }



    // inclisive min/max
    // meow.poke( system.districts, record.districtId, 'counter', {counter: 0, district: record.districtId})
    meow.poke = function(sourceObject, objectKey, incrementKey, initialObject) {
      if (sourceObject[objectKey]) {
        sourceObject[objectKey][incrementKey]++;
      } else {
        sourceObject[objectKey] = initialObject;
        sourceObject[objectKey][incrementKey] = 1;
      }
    }

    meow.ensure = function(sourceObject, objectKey, initialObject) {
      if (sourceObject[objectKey]) {
        // exists, do nothing
      } else {
        // did not exist, create it.
        sourceObject[objectKey] = initialObject;
      }
    }

    meow.clone = function(object) {
      return JSON.parse(JSON.stringify(object));
    }


    meow.ota = function(object) {
      var out = [];
      for (var property in object) {
        if (object.hasOwnProperty(property)) {
          out.push( {key: property, value: object[property] });
        }
      }
      return out;
    }

    meow.otaoi = function(object) {

      var out = [];
      for (var property in object) {
        if (object.hasOwnProperty(property)) {

          let newObject = {id: property};
          let subObject = object[property];

          if(typeof subObject != 'object'){
            throw new Error('the property must be an object');
          }
          out.push( $.extend(true, newObject, subObject ));
        }
      }
      return out;
    }

    /*
    Extract object keys
    */
    meow.ok = function(object) {
      var out = [];

      for (var property in object) {
        if (object.hasOwnProperty(property)) {
          out.push(  property );

        }
      }

      return out;
    }
    meow.ov = function(object) {
      var out = [];

      for (var property in object) {
        if (object.hasOwnProperty(property)) {
          out.push(  object[property] );

        }
      }

      return out;
    }

    meow.flat = function(object) {
      var out = [];

      for (var property in object) {
        if (object.hasOwnProperty(property)) {
          out.push( object[property] );

        }
      }

      console.log('FLAT RETURNING', out)
      return out;
    }


    // inclusive min/max
    meow.random = function(min, max) {
      return Math.floor( Math.random() * (max - parseInt(min) + 1)) + parseInt(min);
    };
    meow.rnd = meow.random;


    if(!global.unrandomSessions) global.unrandomSessions = {};
    meow.unrandom = function(session, min, max) {

        // create session
       if(! unrandomSessions[session]){
         unrandomSessions[session] = { value: min -1 };
       }

       // increment
       unrandomSessions[session].value++;

       // if too high cycle back
       if( unrandomSessions[session].value > max ) unrandomSessions[session].value = min;

       // done
       return unrandomSessions[session].value;

    };

    meow.translateDomain  = function(value, valueDomainMinimum, valueDomainMaximum, targetDomainFloor, targetDominCeiling, flip = false) {

      if(flip){
        let alpha = targetDomainFloor;
        let omega = targetDominCeiling;
        // -------------------------- //
        targetDomainFloor = omega;
        targetDominCeiling = alpha;
      }

      let response = (value - valueDomainMinimum) / (valueDomainMaximum - valueDomainMinimum) * (targetDominCeiling - targetDomainFloor) + targetDomainFloor;
      return response;
    }

    meow.ranger = function(intConditionCout, intValueToRange){
      // intValueToRange must be 0-100;

      let matchingConditionId = "Unknown";

      // partial is the step
      let partialComponent = 100/intConditionCout;

      for(let conditionId = 0; conditionId < intConditionCout;  conditionId++) {

        let lowerLimit = parseInt(partialComponent*conditionId);

        //uses look ahead but it is fence post anyway
        let upperLimit = parseInt(partialComponent*(conditionId+1));

        // must check <0
        if((intValueToRange - lowerLimit)*(intValueToRange - upperLimit)<=0){
          matchingConditionId = conditionId;
          break;
        }

      }
      // returns index
      return matchingConditionId;
    }
    meow.randomItem = function(list){
      var min = 0;
      var max = list.length-1;
      var chosen = meow.random(min, max);
      return list[chosen];
    }

    // http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
    meow.guid = function() {
      function cook() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      }
      return cook() + cook() + '-' + cook() + '-' + cook() + '-' + cook() + '-' + cook() + cook() + cook();
    }


    meow.walk = function(dir, o) {

      var obj = o.obj;
      var prop = o.prop;
      var max = o.max;
      var min = o.min;

      var walk = {
        next: function() {
          console.log('(%d/%d) WALK ->', obj[prop], max);
          obj[prop]++;
          // if went over goto 0
          if (obj[prop] > max) {
            obj[prop] = min;
          }
          // now that selection has changed, force reload.

        },

        prev: function() {
          console.log('WALK <-(%d/%d)', obj[prop], max);
          obj[prop]--;
          // if nent back too far set last
          if (obj[prop] < min) {
            obj[prop] = max;
          }
          // now that selection has changed, force reload.

        }

      }


      walk[dir]();

    }


module.exports = meow;
